<?php

namespace AppBundle\AddressAssigner;

use AppBundle\Entity\Address;
use Sylius\Component\Core\Model\CustomerInterface;
use Sylius\Component\Core\Model\OrderInterface;

final class CustomerOrderAddressAssigner implements OrderAddressAssignerInterface
{
    public function assignAddress(OrderInterface $order): void
    {
        /** @var Address $shippingAddress */
        $shippingAddress = $order->getShippingAddress();

        /** @var CustomerInterface $customer */
        $customer = $order->getCustomer();

        if (null === $shippingAddress || $customer->hasAddress($shippingAddress) || null !== $shippingAddress->getBaseAddress()) {
            return;
        }

        $customer->addAddress($shippingAddress);
    }
}
