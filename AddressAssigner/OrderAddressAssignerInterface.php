<?php

namespace AppBundle\AddressAssigner;

use Sylius\Component\Core\Model\OrderInterface;

interface OrderAddressAssignerInterface
{
    public function assignAddress(OrderInterface $order): void;
}
