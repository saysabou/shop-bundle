<?php


namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;

/**
 * Class SolrImportController
 *
 * @author Vladislav Shishko <vladislav.shishko@infolox.de>
 */
class SolrImportController extends Controller
{
    /**
     * @param Request $request
     *
     * @return Response
     *
     * @throws \Exception
     */
    public function reindexAction(Request $request)
    {
        $this->get('event_dispatcher')->removeListener('kernel.response', [
            $this->get('sylius.listener.session_cart'),
            'onKernelResponse'
        ]);

        $kernel = $this->get('kernel');
        $application = new Application($kernel);
        $application->setAutoExit(false);

        $input = new ArrayInput(array(
            'command' => 'infolox:import',
            '--no-debug',
        ));

        $output = new BufferedOutput();
        $application->run($input, $output);

        $content = $output->fetch();

        return new Response($content);
    }
}
