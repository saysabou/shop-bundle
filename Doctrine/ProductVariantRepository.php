<?php

namespace AppBundle\Doctrine;

use Doctrine\ORM\QueryBuilder;
use Sylius\Bundle\CoreBundle\Doctrine\ORM\ProductVariantRepository as BaseProductVariantRepository;

/**
 * Class ProductVariantRepository
 *
 * @author Vladislav Shishko <vladislav.shishko@infolox.de>
 */
class ProductVariantRepository extends BaseProductVariantRepository
{
    /**
     * @param string $locale
     *
     * @return QueryBuilder
     */
    public function createInventoryListQueryBuilder(string $locale): QueryBuilder
    {
        return $this->createQueryBuilder('o')
            ->leftJoin('o.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->andWhere('o.tracked = true')
            ->setParameter('locale', $locale);
    }

    /**
     * @param string $locale
     * @param        $productId
     *
     * @return QueryBuilder
     */
    public function createQueryBuilderByProductId(string $locale, $productId): QueryBuilder
    {
        return $this->createQueryBuilder('o')
            ->leftJoin('o.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->andWhere('o.product = :productId')
            ->setParameter('locale', $locale)
            ->setParameter('productId', $productId);
    }

    /**
     * @param string $locale
     * @param string $productCode
     *
     * @return QueryBuilder
     */
    public function createQueryBuilderByProductCode(string $locale, string $productCode): QueryBuilder
    {
        return $this->createQueryBuilder('o')
            ->leftJoin('o.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->innerJoin('o.product', 'product')
            ->andWhere('product.code = :productCode')
            ->setParameter('locale', $locale)
            ->setParameter('productCode', $productCode);
    }
}
