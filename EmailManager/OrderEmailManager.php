<?php

namespace AppBundle\EmailManager;


use Sylius\Bundle\ShopBundle\EmailManager\OrderEmailManagerInterface;
use Sylius\Component\Core\Model\OrderInterface;
use Sylius\Bundle\CoreBundle\Mailer\Emails;
use Sylius\Component\Mailer\Sender\SenderInterface;

class OrderEmailManager implements OrderEmailManagerInterface
{
    /**
     * @var SenderInterface
     */
    private $emailSender;

    private $defaultAttachments = [];

    /**
     * @var string
     */
    private $orderConfirmationBccEmail;

    /**
     * @param SenderInterface $emailSender
     * @param string $orderConfirmationBccEmail
     */
    public function __construct(SenderInterface $emailSender, $orderConfirmationBccEmail = null)
    {
        $this->emailSender = $emailSender;
        $this->orderConfirmationBccEmail = $orderConfirmationBccEmail;
    }

    /**
     * {@inheritDoc}
     */
    public function sendConfirmationEmail(OrderInterface $order) : void
    {
        $recipients = [
            $order->getBillingAddress()->getEmail(),
        ];

        $this->emailSender
            ->send(Emails::ORDER_CONFIRMATION,
                $recipients,
                ['order' => $order]
            );
    }
}