<?php

namespace AppBundle\Entity;

use Sylius\Component\Core\Model\Address as BaseAddress;
use Sylius\Component\Core\Model\AddressInterface;

class Address extends BaseAddress
{
    /** @var string */
    protected $gender;
    /** @var string */
    protected $companyId;
    /** @var string */
    protected $fax;
    /** @var string */
    protected $email;

    /** @var AddressInterface|null */
    private $baseAddress;

    /**
     * @return string
     */
    public function getGender(): ?string
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     * @return Address
     */
    public function setGender(string $gender): Address
    {
        $this->gender = $gender;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCompanyId()
    {
        return $this->companyId;
    }

    /**
     * @param mixed $companyId
     */
    public function setCompanyId($companyId): void
    {
        $this->companyId = $companyId;
    }

    /**
     * @return mixed
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * @param mixed $fax
     */
    public function setFax($fax): void
    {
        $this->fax = $fax;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return null|AddressInterface
     */
    public function getBaseAddress(): ?AddressInterface
    {
        return $this->baseAddress;
    }

    /**
     * @param null|AddressInterface $baseAddress
     */
    public function setBaseAddress(?AddressInterface $baseAddress): void
    {
        $this->baseAddress = $baseAddress;
    }
}
