<?php

declare(strict_types=1);

namespace AppBundle\Entity;

use Infolox\SyliusRoleManagementPlugin\Entity\AdjustmentInterface;
use Infolox\SyliusRoleManagementPlugin\Entity\Adjustment as BaseAdjustment;

class Adjustment extends BaseAdjustment implements AdjustmentInterface
{
}
