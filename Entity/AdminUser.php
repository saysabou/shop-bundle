<?php

declare(strict_types=1);

namespace AppBundle\Entity;

use Infolox\SyliusRoleManagementPlugin\Entity\AdminUserInterface;
use Infolox\SyliusRoleManagementPlugin\Entity\AdminUser as BaseAdminUser;

class AdminUser extends BaseAdminUser implements AdminUserInterface
{
    public function isEnablePermissionChecker(): bool
    {
        if (null === $this->enablePermissionChecker) {
            return false;
        }

        return $this->enablePermissionChecker;
    }
}
