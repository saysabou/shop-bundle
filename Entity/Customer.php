<?php

declare(strict_types=1);

namespace AppBundle\Entity;

use Infolox\SyliusRoleManagementPlugin\Entity\CustomerInterface;
use Infolox\SyliusRoleManagementPlugin\Entity\Customer as BaseCustomer;

class Customer extends BaseCustomer implements CustomerInterface
{
}
