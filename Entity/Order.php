<?php

namespace AppBundle\Entity;

use Infolox\SyliusRoleManagementPlugin\Entity\Order as BaseOrder;

class Order extends BaseOrder implements OrderInterface
{
    /** @var int */
    protected $payoneTransaction;

    /** @var int */
    protected $payoneUserId;

    public function getPayoneTransaction(): ?int
    {
        return $this->payoneTransaction;
    }

    public function setPayoneTransaction(?int $payoneTransaction)
    {
        $this->payoneTransaction = $payoneTransaction;
    }

    public function getPayoneUserId(): ?int
    {
        return $this->payoneUserId;
    }

    public function setPayoneUserId(?int $payoneUserId)
    {
        $this->payoneUserId = $payoneUserId;
    }
}
