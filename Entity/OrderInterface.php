<?php

namespace AppBundle\Entity;

use Infolox\SyliusRoleManagementPlugin\Entity\OrderInterface as BaseOrderInterface;

interface OrderInterface extends BaseOrderInterface
{
    public function getPayoneTransaction(): ?int;

    public function setPayoneTransaction(?int $payoneTransaction);

    public function getPayoneUserId(): ?int;

    public function setPayoneUserId(?int $payoneUserId);
}