<?php

declare(strict_types=1);

namespace AppBundle\Entity;

use Infolox\SyliusRoleManagementPlugin\Entity\OrderItem as BaseOrderItem;
use Infolox\SyliusRoleManagementPlugin\Entity\OrderItemInterface;

class OrderItem extends BaseOrderItem implements OrderItemInterface
{
}
