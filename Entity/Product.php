<?php


namespace AppBundle\Entity;

use Sylius\Component\Core\Model\Product as BaseModel;

class Product extends BaseModel
{
    /**
     * @var string
     */
    private $hash;

    /**
     * @return string
     */
    public function getHash(): ?string
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     */
    public function setHash(string $hash): void
    {
        $this->hash = $hash;
    }
}