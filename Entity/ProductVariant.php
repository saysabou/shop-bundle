<?php
/**
 * Created by PhpStorm.
 * User: michi
 * Date: 15.09.17
 * Time: 16:05
 */

namespace AppBundle\Entity;

use Sylius\Component\Core\Model\ProductVariant as BaseModel;

class ProductVariant extends BaseModel
{
    /** @var  string */
    private $sku;

    /** @var  string */
    private $previewImage;

    /**
     * @var array
     */
    private $payload;

    /**
     * @var string
     */
    private $hash;

    /**
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param string $sku
     * @return ProductVariant
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
        return $this;
    }

    /**
     * @return string
     */
    public function getPreviewImage()
    {
        return $this->previewImage;
    }

    /**
     * @param string $previewImage
     * @return ProductVariant
     */
    public function setPreviewImage($previewImage)
    {
        $this->previewImage = $previewImage;
        return $this;
    }

    /**
     * @return array
     */
    public function getPayload(): array
    {
        return $this->payload;
    }

    /**
     * @param array $payload
     */
    public function setPayload(array $payload): void
    {
        $this->payload = $payload;
    }

    /**
     * @return string
     */
    public function getHash(): ?string
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     */
    public function setHash(string $hash): void
    {
        $this->hash = $hash;
    }
}