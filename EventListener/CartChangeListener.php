<?php

namespace AppBundle\EventListener;

use Doctrine\Common\Persistence\ObjectManager;
use Sylius\Component\Core\Model\OrderItemInterface;
use Sylius\Component\Order\Model\OrderInterface;
use Sylius\Component\Order\Processor\OrderProcessorInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Webmozart\Assert\Assert;

class CartChangeListener
{
    /**
     * @var OrderProcessorInterface
     */
    private $orderProcessor;

    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * @param OrderProcessorInterface $orderProcessor
     * @param ObjectManager $objectManager
     */
    public function __construct(OrderProcessorInterface $orderProcessor, ObjectManager $objectManager)
    {
        $this->orderProcessor = $orderProcessor;
        $this->objectManager = $objectManager;
    }

    /**
     * @param GenericEvent $event
     */
    public function recalculateOrderOnAdd(GenericEvent $event): void
    {
        $item = $event->getSubject();
        Assert::isInstanceOf($item, OrderItemInterface::class);
        $order = $item->getOrder();

        $this->orderProcessor->process($order);

        $this->objectManager->persist($order);
    }

    /**
     * @param GenericEvent $event
     */
    public function recalculateOrderOnDelete(GenericEvent $event): void
    {
        $item = $event->getSubject();
        Assert::isInstanceOf($item, OrderItemInterface::class);

        /** @var OrderInterface $order */
        $order = $item->getOrder();
        $order->removeItem($item);

        $this->orderProcessor->process($order);
        $order->recalculateItemsTotal();
        $order->recalculateAdjustmentsTotal();

        $this->objectManager->persist($order);
        $this->objectManager->flush($order);
    }

    /**
     * @param GenericEvent $event
     */
    public function reprocessOrder(GenericEvent $event): void
    {
        $order = $event->getSubject();

        Assert::isInstanceOf($order, OrderInterface::class);

        $this->orderProcessor->process($order);

        $this->objectManager->persist($order);
        $this->objectManager->flush($order);
    }
}
