<?php


namespace AppBundle\Form\Extension;

use Sylius\Bundle\AdminApiBundle\Form\Type\AddressType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormBuilderInterface;
use Sylius\Bundle\AddressingBundle\Form\Type\AddressType as SyliusAddressType;
use Symfony\Component\Validator\Constraints\Valid;

class AddressApiTypeExtension extends AbstractTypeExtension
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $order = $builder->getData();

        $builder
            ->remove('shippingAddress')
            ->remove('billingAddress')
            ->add('shippingAddress', SyliusAddressType::class, [
                'shippable' => true,
                'constraints' => [new Valid()],
                'baseAddress' => true,
                'order' => $order,
            ])
            ->add('billingAddress', SyliusAddressType::class, [
                'constraints' => [new Valid()],
                'baseAddress' => true,
                'order' => $order,
            ])
        ;
    }

    /**
     * Returns the name of the type being extended.
     *
     * @return string The name of the type being extended
     */
    public function getExtendedType()
    {
        return AddressType::class;
    }
}
