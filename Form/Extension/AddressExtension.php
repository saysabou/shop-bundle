<?php


namespace AppBundle\Form\Extension;


use Sylius\Bundle\AddressingBundle\Form\Type\AddressType;
use Sylius\Component\Core\Model\AddressInterface;
use Sylius\Component\Core\Model\CustomerInterface;
use Sylius\Component\Core\Model\OrderInterface;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddressExtension extends AbstractTypeExtension
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var OrderInterface $order */
        $order = $builder->getData();

        $builder->add('gender');
        $builder->add('companyId');
        $builder->add('fax');
        $builder->add('email');

        if (true === $options['baseAddress']) {
            /** @var CustomerInterface $customer */
            $customer = $options['order']->getCustomer();

            $builder
                ->add('baseAddress', ChoiceType::class, [
                    'choices' => $customer->getAddresses(),
                    'choice_value' => function ($value) {
                        /** @var AddressInterface $value */
                        if (null === $value) {
                            return $value;
                        }

                        return $value->getId();
                    }
                ])
            ;
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);

        $resolver
            ->setDefault('baseAddress', false)
            ->setDefault('order', false)
        ;
    }

    /**
     * Returns the name of the type being extended.
     *
     * @return string The name of the type being extended
     */
    public function getExtendedType()
    {
        return AddressType::class;
    }
}
