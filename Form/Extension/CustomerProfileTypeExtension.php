<?php

declare(strict_types=1);

namespace AppBundle\Form\Extension;

use Infolox\SyliusRoleManagementPlugin\Form\Type\CustomerRoleChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormBuilderInterface;
use Sylius\Bundle\CustomerBundle\Form\Type\CustomerProfileType;

final class CustomerProfileTypeExtension extends AbstractTypeExtension
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('customerRole', CustomerRoleChoiceType::class, [
                'required' => true,
            ])
        ;
    }

    public function getExtendedType(): string
    {
        return CustomerProfileType::class;
    }
}
