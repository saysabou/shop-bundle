<?php
declare(strict_types=1);

namespace AppBundle\Form\Extension;

use Sylius\Bundle\OrderBundle\Form\Type\OrderType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;

final class OrderNotesExtension extends AbstractTypeExtension
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('notes', TextareaType::class, [
            'required' => false
        ]);
    }

    public function getExtendedType()
    {
        return OrderType::class;
    }
}